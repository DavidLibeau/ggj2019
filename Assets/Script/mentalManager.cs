﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mentalManager : MonoBehaviour
{
    public bool mentalIssue=false;

    public int mentalType=0;

    public float timeBeforeIssue = 0f;
    private float timeNextIssue = 0f;
    private float timeStartIssue = 0f;

    private float holdKeyStartTime = 0f;
    public float holdKeyTime = 1.0f;

    private bool collidingPlayer = false;
    private Animator anim = null;

    public GameObject interactionHelper;

    void Start()
    {
        resetMentalIssue();
        anim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        timeBeforeIssue = (timeStartIssue + timeNextIssue) - Time.time;
        if (mentalIssue)
        {
            //anim.SetBool("isWalking", false);
            if (collidingPlayer)
            {
                switch (mentalType)
                {
                    case 1:
                        interactionHelper.GetComponent<Text>().text = "Press E to help";
                        interactionHelper.SetActive(true);
                        if (Input.GetButtonDown("Interact"))
                        {
                            noMentalIssue();
                        }
                        break;
                    case 2:
                        interactionHelper.GetComponent<Text>().text = "Hold E to help";
                        interactionHelper.SetActive(true);
                        if (Input.GetButtonDown("Interact"))
                        {
                            holdKeyStartTime = Time.time;
                        }
                        if (Input.GetButton("Interact"))
                        {
                            if (holdKeyStartTime + holdKeyTime <= Time.time)
                            {
                                noMentalIssue();
                            }
                        }
                        break;
                    case 3:
                        interactionHelper.GetComponent<Text>().text = "Press Space to help";
                        interactionHelper.SetActive(true);
                        if (Input.GetButtonDown("Jump"))
                        {
                            noMentalIssue();
                        }
                        break;
                    case 4:
                        interactionHelper.GetComponent<Text>().text = "Hold Space to help";
                        interactionHelper.SetActive(true);
                        if (Input.GetButtonDown("Jump"))
                        {
                            holdKeyStartTime = Time.time;
                        }
                        if (Input.GetButton("Jump"))
                        {
                            if (holdKeyStartTime + holdKeyTime <= Time.time)
                            {
                                noMentalIssue();
                            }
                        }
                        break;
                }
            }
            else
            {
                interactionHelper.SetActive(false);
            }

        }
        else
        {
            interactionHelper.SetActive(false);
        }


        if (timeNextIssue+timeStartIssue <= Time.time)
        {
            Debug.Log("Mental issue : " + mentalType);
            mentalIssue = true;
            gameObject.GetComponent<FollowPlayer>().enabled=false;
            gameObject.GetComponent<ai>().enabled = true;
        }
    }

    private void noMentalIssue()
    {
        mentalIssue = false;
        gameObject.GetComponent<FollowPlayer>().enabled = true;
        gameObject.GetComponent<ai>().enabled = false;
        resetMentalIssue();
        interactionHelper.SetActive(true);
    }

    private void resetMentalIssue()
    {
        timeNextIssue = Random.Range(20f, 50f);
        mentalType = Mathf.RoundToInt(Random.Range(1f, 4f));
        timeStartIssue = Time.time;
    }

    void OnTriggerEnter(Collider coll)
    {
        //Debug.Log(coll.gameObject.tag);
        if (coll.gameObject.tag == "Player")
        {
            collidingPlayer = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        //Debug.Log(coll.gameObject.tag);
        if (coll.gameObject.tag == "Player")
        {
            collidingPlayer = false;
        }
    }
}

