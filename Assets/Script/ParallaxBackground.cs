﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour {

	/*public GameObject camera;
	public float multiplier;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(camera.transform.position.x*multiplier, 0.0f, 0.0f);
	}*/


    public Transform target;
    public float multiplier=1.5f;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    public float height=0f;
    public float depth = 40f;
    public float offsetLeft = -20f;

    void Update()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition = target.TransformPoint(new Vector3((- target.transform.position.x / multiplier)- offsetLeft, height, depth));

        // Smoothly move the camera towards that target position
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }
}
