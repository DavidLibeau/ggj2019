﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class changeScene : MonoBehaviour
{
    private string sceneName="";
    // Start is called before the first frame update
    void Start()
    {
        //changeTo(sceneName);
    }

    // Update is called once per frame
    void Update()
    {
        if (sceneName != "")
        {
            changeTo(sceneName);
        }
    }

    public void changeTo(string sceneName)
    {
        SceneManager.LoadScene(sceneName: sceneName);

    }
}
