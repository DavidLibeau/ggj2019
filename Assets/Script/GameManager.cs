﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static string lang;
	public static float volume;

	public Dropdown dropdownLanguage;
	public Slider sliderVolume;

	public static GameManager Instance;

	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		if (lang == "") {
			lang = "en";
		}
	}
	
	// Update is called once per frame
	void Update () {
		string language = dropdownLanguage.GetComponent<Dropdown> ().options[dropdownLanguage.GetComponent<Dropdown> ().value].text;
		if (language == "English") {
			lang = "en";
		} else if (language == "Français") {
			lang = "fr";
		} else {
			lang = "none";
		}

		volume=sliderVolume.value;

		GameObject[] audios = GameObject.FindGameObjectsWithTag("Audio");
		foreach (GameObject a in audios)
		{
			a.GetComponent<AudioSource> ().volume = volume;
		}
	}
}
