﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform target;
    public float smoothTime = 0.3f;
    private Vector3 velocity = Vector3.zero;
    public Vector3 transformPoint=new Vector3(0, 5, -10);

    private Animator anim=null;

    void Awake()
    {
        if (gameObject.GetComponent<Animator>())
        {
            anim = gameObject.GetComponent<Animator>();
        }
    }

    void Update()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition = target.TransformPoint(transformPoint);

        // Smoothly move the camera towards that target position
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        if (anim)
        {
            if (velocity.x > 1)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
                anim.SetBool("isWalking", true);
            }
            else if (velocity.x < -1)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
                anim.SetBool("isWalking", true);
            }
            else
            {
                anim.SetBool("isWalking", false);
            }
        }

    }

}
