﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ai : MonoBehaviour
{

    [HideInInspector] public bool facingRight = false;
    public float moveForce = 20f;
    public float maxSpeed = 20f;
    
    private Animator anim;
    private Rigidbody rb;

    [Range(-1f, 1f)] public float fakeHorizontal = 0f;

    private float timeStart;
    private float timeNext;

    [Range(0f, 1f)]  public float idleChance=0.5f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (timeStart+timeNext<=Time.time)
        {
            timeNext = Random.Range(1f, 5f);
            timeStart = Time.time;
            float chance = Random.Range(0f, 1f);
            if (chance < idleChance)
            {
                fakeHorizontal = 0;
            }
            else
            {
                fakeHorizontal = Random.Range(0.5f, 1f);
                if(Mathf.RoundToInt(Random.Range(0f, 1f))==1){
                    fakeHorizontal = fakeHorizontal * -1;
                }
            }
        }
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.right * fakeHorizontal * Time.deltaTime * moveForce);
        if (fakeHorizontal != 0f)
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        if (fakeHorizontal > 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (fakeHorizontal < 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

    }

}
