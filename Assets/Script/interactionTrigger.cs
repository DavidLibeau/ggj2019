﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class interactionTrigger : MonoBehaviour
{
    public GameObject canvas;
    public GameObject interactionHelper;
    public string interactionHelperText;

    public bool autoTrigger = false;

    [Header("Action")]
    public bool hideItself = false;
    public bool showSprite=false;
    public bool hideSprite=false;
    public GameObject sprite;
    public bool dialogue=false;
    public string dialogueId;
    public bool changeScene = false;
    public string sceneName;

    [SerializeField]
    public UnityEvent runThat;

    private bool alreadyTriggered;

    // Use this for initialization
    void Start()
    {
        interactionHelper.SetActive(false);
        alreadyTriggered = false;
        if (showSprite)
        {
            sprite.SetActive(false);
        }
        else if (hideSprite)
        {
            sprite.SetActive(true);

        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider coll)
    {
        //Debug.Log(coll.gameObject.tag);
        if (coll.gameObject.tag == "Player" && !alreadyTriggered)
        {
            if (autoTrigger)
            {
                doTrigger();
            }
            else
            {
                if (interactionHelperText != "")
                {
                    interactionHelper.GetComponent<Text>().text = interactionHelperText;
                }
                else
                {
                    interactionHelper.GetComponent<Text>().text = "Press E to interact";
                }
                interactionHelper.SetActive(true);
            }
        }
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.tag == "Player" && !alreadyTriggered)
        {
            if (Input.GetButtonDown("Interact"))
            {
                doTrigger();
            }
        }
    }

    void OnTriggerExit(Collider coll)
    {
        interactionHelper.SetActive(false);
    }

    void doTrigger()
    {
        alreadyTriggered = true;
        interactionHelper.SetActive(false);
        if (hideItself)
        {
            gameObject.SetActive(false);
        }
        if (showSprite)
        {
            sprite.SetActive(true);
        }
        else if (hideSprite)
        {
            sprite.SetActive(false);

        }
        if (dialogue)
        {
            canvas.GetComponent<dialogueManager>().startDialogue(dialogueId);
        }
        if (changeScene)
        {
            SceneManager.LoadScene(sceneName: sceneName);
        }
        runThat.Invoke();
    }

}