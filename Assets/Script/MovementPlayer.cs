﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpriteList
{
	public Sprite Attack, Normal;
}

public class MovementPlayer : MonoBehaviour {

	[HideInInspector] public bool facingRight = false;
	public float moveForce = 40f;
	public float maxSpeed = 20f;
    
	private Animator anim;
	private Rigidbody rb;

	//public GameObject footstep;


	// Use this for initialization
	void Awake () 
	{
		
		rb = GetComponent<Rigidbody>();
		anim = gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () 
	{
		float h = Input.GetAxis("Horizontal");

		if (h!=0 && Time.timeScale!=0){
			//Debug.Log("animation to go walk");
			anim.SetBool ("isWalking", true);
			//footstep.GetComponent<Footstep> ().playFootstep ();
		}
		else{
			anim.SetBool ("isWalking", false);
		}

	}


	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");

		//anim.SetFloat("Speed", Mathf.Abs(h));
		RaycastHit hit;
		if (rb.SweepTest (new Vector3 (h, 0), out hit, 0.5f, QueryTriggerInteraction.UseGlobal))
		{
			// If so, stop the movement
			rb.velocity = new Vector3(0, rb.velocity.y, 0);
		}

		transform.Translate (Vector3.right * h * Time.deltaTime * moveForce);
					
		if (h > 0 && !facingRight && Time.timeScale!=0){
			Flip ();
		}			
		else if (h < 0 && facingRight){
			Flip ();
		}
			
	}


	void Flip()
	{
		facingRight = !facingRight;
        gameObject.GetComponent<SpriteRenderer>().flipX = !gameObject.GetComponent<SpriteRenderer>().flipX;
	}
}