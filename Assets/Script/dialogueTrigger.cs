﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueTrigger : MonoBehaviour {

	public GameObject canvas;
	public string dialogue;

	private bool alreadyTriggered;

	// Use this for initialization
	void Start () {
		alreadyTriggered = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider coll){
		//Debug.Log(coll.gameObject.tag);
		if (coll.gameObject.tag == "Player" && !alreadyTriggered) {
			canvas.GetComponent<dialogueManager> ().startDialogue (dialogue);
			alreadyTriggered = true;
		}
	}


}
