﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;


public class dialogueManager : MonoBehaviour {

	private XmlNode xml;
	private bool dialogueActive;
	private string currentDialogue;
	private int currentSentence;
	public GameObject youngGroup;
	public GameObject oldGroup;
	public Text sentenceTextboxYoung;
	public Text sentenceTextboxOld;

    private Text activeTextObject=null;
    private string activeText="";
    private bool cancelLastText = false;


    void loadDialogue (int sentence) {
		string sentenceXpath = "//dialogue[@id=\"" + currentDialogue + "\"]/sentence[" + sentence + "]/" + GameManager.lang;
		string groupXpath = "//dialogue[@id=\"" + currentDialogue + "\"]/sentence[" + sentence + "]/@type";
		if (xml.SelectNodes (sentenceXpath).Count == 0) {
			Debug.Log ("End of dialogue "+currentDialogue);
			Time.timeScale=1;
			youngGroup.SetActive(false);
			oldGroup.SetActive(false);
			dialogueActive = false;
		} else {
			if (xml.SelectSingleNode (groupXpath).InnerText == "young") {
				youngGroup.SetActive(true);
				oldGroup.SetActive(false);
                Debug.Log(xml.SelectSingleNode(sentenceXpath).InnerText);
                sentenceTextboxYoung.text = "";
                StartCoroutine(typeText(sentenceTextboxYoung, xml.SelectSingleNode(sentenceXpath).InnerText));
            }
            else if (xml.SelectSingleNode (groupXpath).InnerText == "old" || xml.SelectSingleNode(groupXpath).InnerText == "doctor") {
				youngGroup.SetActive(false);
				oldGroup.SetActive(true);
                Debug.Log(xml.SelectSingleNode(sentenceXpath).InnerText);
                sentenceTextboxOld.text = "";
                StartCoroutine(typeText(sentenceTextboxOld, xml.SelectSingleNode(sentenceXpath).InnerText));
            }
            else {
				Debug.Log ("XML error: Type '"+xml.SelectSingleNode (groupXpath).InnerText+"' is wrong");
				youngGroup.SetActive(false);
				oldGroup.SetActive(false);
			}
		}
	}

	public void startDialogue (string id) {
		//Debug.Log ("Start dialogue : " + id);
		if (GameManager.lang != "none") {
			dialogueActive = true;
			this.currentDialogue = id;
			currentSentence = 1;
			loadDialogue (currentSentence);

			//Time.timeScale=0;
		}
	}

	void Start() {
        TextAsset rawXML = (TextAsset)Resources.Load("dialogues");

        dialogueActive = false;

		XmlDocument doc = new XmlDocument ();
        //doc.Load (UnityEngine.Application.dataPath + "/dialogues.xml");
        doc.LoadXml(rawXML.text);
        xml = doc.FirstChild;

        youngGroup.SetActive(false);
        oldGroup.SetActive(false);
    }

	void Update () {

		if (dialogueActive) {
			if (Input.GetButtonDown ("Fire1") || Input.GetButtonDown("Jump")) {
                //StopCoroutine(typeText);
                cancelLastText = true;
                currentSentence++;
				loadDialogue (currentSentence);
			}
		}
	}

    IEnumerator typeText(Text textObject, string text)
    {
        textObject.text = "";
        yield return 0;
        yield return new WaitForSeconds(0.1f);
        cancelLastText = false;
        foreach (char letter in text.ToCharArray())
        {
            textObject.text += letter;
            /*if (typeSound1 && typeSound2)
                SoundManager.instance.RandomizeSfx(typeSound1, typeSound2);*/
            if (!cancelLastText)
            {
                yield return 0;
                yield return new WaitForSeconds(0.01f);
            }
            else
            {
                textObject.text = text;
            }
        }
    }
}
