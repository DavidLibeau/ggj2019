﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LanguageControl : MonoBehaviour {

    public void NextScene()
    {
        SceneManager.LoadScene(sceneName: "Language");
    }
}